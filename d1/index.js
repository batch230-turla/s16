console.log("Hello World");

// Aritmetic Operators
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition Operator: " + sum);

let difference = x - y;
console.log("Result of subraction Operator: " + difference);

let product = x * y;
console.log("Result of multiplication Operator: " + product);

//Qoutient

let qoutient = x / y;
console.log("Result of division Operator: " + qoutient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

//Assignment Operator ( = )

	// Basic Assignment Operator (=)
    // The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8; //initializing

assignmentNumber = assignmentNumber + 2; //10 // Reassigning through using addtion operator
console.log("Result of addtion operator:" + assignmentNumber);

//addition assignment operator

assignmentNumber +=  2; // 12
console.log("Result of addtion assignment operator:" + assignmentNumber);

assignmentNumber -=  2; // 10
console.log("Result of subraction assignment operator:" + assignmentNumber);


assignmentNumber *=  2; //20
console.log("Result of multiplication assignment operator:" + assignmentNumber);


assignmentNumber /=  2; // 10
console.log("Result of division assignment operator:" + assignmentNumber);

//Multiple Operators and Parenthesis

//MDAS (multiplication, division, addition, subtraction)


/*
            - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6
        */

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of MDAS operator: " + mdas);

// The order of operations can be changed by addin parenthesis
//PEMDAS (parenthesis, exponents, multiplication, division, addition, subtraction)

/*
            - By adding parentheses "()" to create more complex computations will change the order of operations still following the same rule.
            - The operations were done in the following order:
                1. 4 / 5 = 0.8
                2. 2 - 3 = -1
                3. 1 + -1 = 0
                4. 0 * 0.8 = 0
        */


let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of PEMDAS operator: " + pemdas);

//Increment and Decrement

let z = 1;

//pre- increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment (z): " + z);

// post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment (z): " + z);

// increment = z++;
// console.log(increment);

// pre-drecement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement (z): " + z);

// post-decrement
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement (z): " + z);

//Type coercion
/*
      - Type coercion is the automatic or implicit conversion of values from one data type to another
      - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
      - Values are automatically converted from one data type to another in order to resolve operations
        */
// string and number
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);


let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion); //30
console.log(typeof nonCoercion); //number

//boolean and Number

let numE = true + 1;
console.log(numE); // 2 
console.log(typeof numE); // number


let numF = false + 1;
console.log(numF);
console.log(typeof numF);

//Equality Operator(==)
// we could read out equality operator with  'is equal'
/* 
    - Checks whether the operands are equal/have the same content
    - Attempts to CONVERT AND COMPARE operands of different data types
    - Returns a boolean value
*/

let juan = 'juan';

console.log('----------------')
console.log(1==1); // true
console.log(1==2); // false
console.log(1=='1'); // true
console.log('juan'=='juan'); //true
console.log(juan == 'juan'); // true

//Inequality operator (!=)
//Not equal
console.log('----------------');
console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 != '1'); // false
console.log('juan' != 'juan'); // false
console.log('juan' != juan); // false

//Strict Equality Operator (===)

/* 
    - Checks whether the operands are equal/have the same content
    - Also COMPARES the data types of 2 values
    - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
    - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
    - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
    - Strict equality operators are better to use in most cases to ensure that data types provided are correct

*/
console.log('----------------');
console.log(1 === 1); //true
console.log(1 === 2); // false 
console.log(1 === '1'); // false
console.log(0 === false); // false 
console.log('juan' === 'juan'); // true
console.log(juan === 'juan'); // true

// Strict Inequality operator.
        /* 
            - Checks whether the operands are not equal/have the same content
            - Also COMPARES the data types of 2 values
        */
console.log('----------------');
console.log(1 !== 1); //false
console.log(1 !== 2); // true 
console.log(1 !== '1'); // true
console.log(0 !== false); // true 
console.log('juan' !== 'juan'); // false
console.log(juan !== 'juan'); // false

//Relational Operators
//Some comparison operators check whether one value is greater or less than to the other value.

let a = 50;
let b = 65;

let isGreaterThan = a > b; //false
let isLessThan = a < b; //true
let isGTorEqual = a >= b; //false
let isLTorEqual = a <= b; //true

console.log('----------------');
console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

//Logical Operator (&& - AND, || - OR, ! - NOT)

let isLegalAge = true;
let isRegistered = true;
let isMarried = false;

//Logical and Operator (&&)
let isLegalAgeisRegistered = isLegalAge && isRegistered;
console.log("Result of Logical And Operator " + isLegalAgeisRegistered); //true

let allCondition = isLegalAge && isRegistered && isMarried; //false
console.log("Result of Logical And Operator " + allCondition);

// Logical Or Operator (||)
// Even just one is true or satisfied the output will be true
let someRequirements = isLegalAge || isMarried;
console.log("Result of Logical OR operator: " + someRequirements);


//Logical Not Operator
let someRequirementsNotMet = !isRegistered; // false;
console.log("Result of Logical Not Operator: " + someRequirementsNotMet); 

